﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SignUpPage.Models;
using Microsoft.EntityFrameworkCore;

namespace SignUpPage.Data
{
    public class MyContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public MyContext(DbContextOptions<MyContext> options) : base(options)
        {

        }        
    }
}

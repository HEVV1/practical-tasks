﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SignUpPage.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using SignUpPage.Data;

namespace SignUpPage.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly MyContext _myContext;

        public HomeController(ILogger<HomeController> logger, MyContext context)
        {
            _logger = logger;
            _myContext = context;            
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignUp(User user)
        {
            if (ModelState.IsValid && user.IsTermsAccepted == true)
            {
                _myContext.Add(user);
                await _myContext.SaveChangesAsync();
                return RedirectToAction(nameof(LogIn));
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Accept Terms of condition");
            }
            return View(user);
        }

       
        public IActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogIn(ViewLoginModel login)
        {
            
            if (ModelState.IsValid)
            {
                if (_myContext.Users.Where(m => m.Email == login.Email && m.Password == login.Password).FirstOrDefault() == null)
                {
                    ModelState.AddModelError(string.Empty, "Invalid login Attempt");
                    return View();
                }
                else
                {
                    return RedirectToAction(nameof(AfterLogIn));
                }                               
            }
            return View(login);
        }
       

        public IActionResult AfterLogIn()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SignUpPage.Models
{
    public class User
    {
        public int ID { get; set; }        

        [Display(Name ="First Name")]
        [Required(ErrorMessage = "Please enter the First Name")]
        public string FirstName { get; set; }


        [Required(ErrorMessage = "Please enter the Last Name")]
        public string LastName { get; set; }


        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Please enter the e-mail")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Please enter the Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Password Confirm")]
        [Required(ErrorMessage = "Please Confirm the password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Your password and confirm password don't match")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please Confirm the password")]
        public string Street { get; set; }


        [Required(ErrorMessage = "Please Confirm the city")]
        public string City { get; set; }


        [Required(ErrorMessage = "Please Confirm the country")]
        public string Country { get; set; }


        [Required(ErrorMessage = "Please Confirm the postCode")]
        public string PostCode { get; set; }
        
        
        public bool IsTermsAccepted { get; set; }
    }
}
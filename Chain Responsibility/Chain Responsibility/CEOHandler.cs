﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chain_Responsibility
{
    class CEOHandler : AbstractHandler
    {
        public override object Handle(object request)
        {
            if (Convert.ToInt32(request) == 100000)
            {
                return "CEO approve cost: " + request;
            }
            else
            {
                return base.Handle(request);
            }            
        }
    }
}

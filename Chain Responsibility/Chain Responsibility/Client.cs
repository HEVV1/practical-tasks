﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chain_Responsibility
{
    class Client
    {        
        public static void ClientCode(AbstractHandler handler, List<int> list)
        {
            foreach (var item in list)
            {
                var result = handler.Handle(item); //Applying to variable handle result with particular request which from some Handler
                if (result != null)
                {
                    Console.WriteLine(result); //Print handle result
                }                
            }
        }
    }
}

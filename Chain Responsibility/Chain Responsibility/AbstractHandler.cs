﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chain_Responsibility
{
    class AbstractHandler : IHandler
    {
        //This vatriable works as a link to make a chain work, same principal works in List where is using so called nodes
        private IHandler _nextHandler;
                
        public IHandler SetNext(IHandler handler) //Method that actually helps as to create a chain with handlers
        {
            _nextHandler = handler;
            return handler;
        }
        //This method helps us to walk through method if some particular Handler not matched a request
        public virtual object Handle(object request) 
        {
            if (_nextHandler != null)
            {
                return _nextHandler.Handle(request);
            }
            else
            {
                return null;
            }
        }
    }
}

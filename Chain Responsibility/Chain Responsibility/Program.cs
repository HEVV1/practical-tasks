﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chain_Responsibility
{
    class Program
    {
        static List<int> salary = new List<int>() { 500, 2000, 5000, 20000, 100000 };
        static void Main(string[] args)
        {            
            ProjectManagerHandler project = new ProjectManagerHandler();
            ProgramManagerHandler program = new ProgramManagerHandler();
            SubdivisionManagerHandler subdivision = new SubdivisionManagerHandler();
            DivisionDirectorHandler director = new DivisionDirectorHandler();
            CEOHandler ceo = new CEOHandler();
            //Making a chain using SetNext method in each Handler
            project.SetNext(program).SetNext(subdivision).SetNext(director).SetNext(ceo);

            Console.WriteLine("Salary distribution: ");
            Client.ClientCode(project, salary); //Since Client is static we needn't to create an instance of Client class

            Console.ReadLine();
        }
    }
}

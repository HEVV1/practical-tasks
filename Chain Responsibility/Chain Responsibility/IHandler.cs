﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chain_Responsibility
{
    //Just a simple interface with bunch of methods
    interface IHandler
    {
        IHandler SetNext(IHandler handler);
        object Handle(object request);
    }
}

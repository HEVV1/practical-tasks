﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chain_Responsibility
{
    class ProjectManagerHandler: AbstractHandler
    {
        public override object Handle(object request)
        {
            if (Convert.ToInt32(request) == 500)
            {
                return "Project manager approve cost: " + request;
            }
            else
            {
                return base.Handle(request);
            }
        }
    }
}

--Select the last policy version for each policy
SELECT policy_version_id, policy_version_number, policy_id
FROM policy_version
INNER JOIN policy ON policy_version.policy_id=policy.policy_id
ORDER BY policy_version_id DESC LIMIT 1;

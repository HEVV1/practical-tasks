﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coding
{
    class Program
    {

        static string secretNumber { get; set; } = "";
        static void Main(string[] args)
        {
            SecretNumber();            
            PlayerGuess(secretNumber);
            Console.ReadLine();
        }
        static void SecretNumber()
        {
            Random rnd = new Random();
            int counter = 0;
            do
            {
                counter++;
                int randomVal;
                randomVal = rnd.Next(1, 10);
                if (secretNumber.IndexOf(randomVal.ToString()) == -1)
                {
                    secretNumber += randomVal;
                }
                else
                {
                    randomVal = rnd.Next(1, 10);
                }

            } while (secretNumber.Length < 4);            
        }
        static void PlayerGuess(string number)
        {            
            char[] numberArray = number.ToCharArray();
            int m = 0;
            int p = 0;
            int counter = 0;
            
            while ((counter != 8) && (p != 4))
            {
                m = 0;
                p = 0;
                counter++;                
                Console.Write("Guess the number: ");
                string guess = Console.ReadLine();
                char[] guessArray = guess.ToCharArray();
                for (int d = 0; d < guessArray.Length; d++)
                {
                    if (numberArray.Contains(guessArray[d]))
                    {
                        m++;
                    }
                }
                for (int s = 0; s < numberArray.Length; s++)
                {
                    if (numberArray[s] == (guessArray[s]))
                    {
                        p++;
                        m--;
                    }
                }
                Console.WriteLine("M: " + m);
                Console.WriteLine("P: " + p);                
            }
            Console.WriteLine();
            if (p == 4)
            {
                Console.WriteLine("You guessed correct, secret number is: "+ secretNumber);
            }
            else
            {
                Console.WriteLine("You lose, secret number is: " + secretNumber);
            }
        }
    }
}
